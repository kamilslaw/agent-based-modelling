const app = new Vue({
  el: "#app",

  data() {
    return {
      // simulation
      width: 168,
      height: 65,
      pos: null,
      iteration: 0,
      animalsEnergy: {},
      plantsEnergy: {},
      plantsSumEnergy: 0,
      plantsMaxEnergy: 0,
      isPaused: true,
      SPS: 30, // steps per second

      // parameters
      solarPower: 0.5,
      animals: {
        // Rat: { id: 1, level: 1, color: "#aaaaaa", eats: ["Grass", "Cereal"], startPopulation: 50 },
        Hare: { id: 2, level: 1, color: "#88c4c3", eats: ["Grass"], startPopulation: 400 },
        Fox: { id: 3, level: 2, color: "#f86b3e", eats: ["Hare"], startPopulation: 120 },
        // Eagle: { id: 4, level: 2, color: "#825a2c", eats: ["Rat", "Hare"], startPopulation: 10 },
        Wolf: { id: 5, level: 3, color: "#ff00ff", eats: ["Hare", "Fox"], startPopulation: 40 },
        // Cormorant: { id: 6, level: 3, color: "#003366", eats: ["Rat", "Hare", "Fox", "Eagle"], startPopulation: 25 },
        Lion: { id: 7, level: 4, color: "#3322dd", eats: ["Hare", "Fox", "Wolf"], startPopulation: 20 },
        // Hydra: { id: 7, level: 4, color: "#D0A030", eats: ["Rat", "Fox", "Eagle", "Wolf", "Cormorant"], startPopulation: 10 },
        Dragon: { id: 8, level: 5, color: "#FF0033", eats: ["Hare", "Fox", "Wolf", "Lion"], startPopulation: 8 }
      },
      plants: {
        // Cereal: { id: 101, color: "#EFE234" },
        Grass: { id: 102, color: "#19A50D" }
      },

      // visualisation
      backgorundColor: "#19a50d",
      ratio: 10,
      pixi: null,
      graphics: null,
      FPS: 5, 
      chartStep: 6,
      chartIteration: 0,
      drawIteration: 0,
      showProducers: false,
      showWholeChart: true,
      chartLength: 200,

      // performance
      lastUpdateDuration: 0,
      averageUpdateDuration: 0,
      lastPlotDuration: 0,
      averagePlotDuration: 0,
      lastDrawDuration: 0,
      averageDrawDuration: 0,
      totalTime: 0
    };
  },

  mounted() {
    this.animalsKeys = Object.keys(this.animals);
    this.plantsKeys = Object.keys(this.plants);

    this.initPositions();
    this.initVisualisation();
    this.startSimulation();
  },

  methods: {
    initPositions() {
      this.pos = this.create2DArray(this.height);

      const animals = Object.entries(this.animals);
      animals.forEach(a => {
        this.animals[a[0]].population = a[1].startPopulation;
        this.animalsEnergy[a[0]] = [a[1].startPopulation * this.getStartEnergy(a[1].level)];
      });

      const plants = Object.entries(this.plants);
      plants.forEach(a => this.plantsEnergy[a[0]] = [this.getStartEnergy(0) * this.width * this.height / plants.length]);

      this.plantsMaxEnergy = this.plantsSumEnergy = this.getStartEnergy(0) * this.width * this.height;

      let s = 0;
      let k = 0;
      const plantChangeTreshold = this.width * this.height / plants.length;
      let p = 0;
      let pIndex = 0;
      for (let i = 0; i < this.height; i++) {
        for (let j = 0; j < this.width; j++) {
          p += 1;
          if (p > plantChangeTreshold) {
            p = 0;
            pIndex += 1;
          }
          const plant = { name: plants[pIndex][0], id: plants[pIndex][1].id, energy: this.getStartEnergy(0) };

          if (s < animals.length) {
            const residents = [
              {
                color: animals[s][1].color,
                level: animals[s][1].level,
                name: animals[s][0],
                id: animals[s][1].id,
                eats: animals[s][1].eats.map(e => this.animals[e] !== undefined ? this.animals[e].id : this.plants[e].id),
                energy: this.getStartEnergy(animals[s][1].level),
                lastIteration: 0,
                ownIteration: Math.round(Math.random() * 50)
              }
            ];
            this.pos[i][j] = { plant, residents };
            k++;
            if (k === animals[s][1].startPopulation) {
              s++;
              k = 0;
            }
          } else {
            this.pos[i][j] = { plant, residents: [] };
          }
        }
      }

      // Shuffle animals
      for (let i = 0; i < this.height; i++) {
        for (let j = 0; j < this.width; j++) {
          let i2 = Math.floor(Math.random() * this.height);
          let j2 = Math.floor(Math.random() * this.width);

          let tmp = this.pos[i][j];
          this.pos[i][j] = this.pos[i2][j2];
          this.pos[i2][j2] = tmp;
        }
      }
    },

    initVisualisation() {
      this.pixi = new PIXI.Application(this.width * this.ratio, this.height * this.ratio, {
        antialias: false,
        backgroundColor: this.hexToVBColor("#000000")
      });
      document.getElementById("draw").appendChild(this.pixi.view);

      this.graphics = new PIXI.Graphics();
      this.pixi.stage.addChild(this.graphics);

      this.updateVisualisation(true);
    },

    startSimulation() {
      this.update();
      this.updateVisualisation();
      this.refreshPlot();
    },

    updateVisualisation(force) {
      if (force || !this.isPaused) {
        const startTime = performance.now();

        this.drawIteration += 1;

        this.graphics.clear();        
        this.graphics.beginFill(this.hexToVBColor(this.backgorundColor), this.plantsSumEnergy / this.plantsMaxEnergy);
        this.graphics.drawRect(0, 0, this.width * this.ratio, this.height * this.ratio);
        for (let i = 0; i < this.height; i++) {
          for (let j = 0; j < this.width; j++) {
            if (this.pos[i][j].residents.length > 0) {
              this.graphics.beginFill(this.hexToVBColor(this.pos[i][j].residents[0].color), 1);
              this.graphics.drawRect(
                j * this.ratio,
                i * this.ratio,
                this.ratio,
                this.ratio
              );
            }
          }
        }

        this.lastDrawDuration = performance.now() - startTime;
        this.averageDrawDuration = (this.averageDrawDuration * (this.drawIteration - 1) + this.lastDrawDuration) / this.drawIteration;
      }

      if (this.FPS > 0) setTimeout(this.updateVisualisation, 1000 / this.FPS);
    },

    update() {
      if (!this.isPaused) {
        const startTime = performance.now();

        this.iteration += 1;

        const drawPlot = this.iteration % this.chartStep === 0;

        let animalsPopulation = {};
        let animalsEnergy = {};
        let plantsEnergy = {}

        for (let y = 0; y < this.height; y++) {
          for (let x = 0; x < this.width; x++) {
            const pos = this.pos[y][x];

            if (pos.plant.energy < this.getStartEnergy(0)) {
              pos.plant.energy += this.addSolarEnergy();
            }

            plantsEnergy[pos.plant.name] = plantsEnergy[pos.plant.name] ? plantsEnergy[pos.plant.name] + pos.plant.energy : pos.plant.energy; 

            for (let i = pos.residents.length - 1; i >= 0; i -= 1) {
              if (pos.residents[i].lastIteration >= this.iteration)
                continue;

              const animal = pos.residents[i];
              animal.lastIteration += 1;
              animal.ownIteration += 1;
              animal.energy -= this.useEnergy(animal.level);

              if (animal.energy <= 0) {
                pos.residents.pop();
                continue;
              } else {
                animalsPopulation[animal.name] = animalsPopulation[animal.name] ? animalsPopulation[animal.name] + 1 : 1;
              }

              if (drawPlot)
                animalsEnergy[animal.name] = animalsEnergy[animal.name] ? animalsEnergy[animal.name] + animal.energy : animal.energy;

              const { newY, newX } = this.getNewPosition(y, x, animal.level, animal.ownIteration);
              if (y !== newY || x !== newX) {
                pos.residents.splice(i, 1);
                this.pos[newY][newX].residents.push(animal);
              }
              
              if (animal.energy < this.getStartEnergy(animal.level)) {
                let animalAte = false;
                for (let j = this.pos[newY][newX].residents.length - 1; j >= 0; j -= 1) {
                  const opponentId = this.pos[newY][newX].residents[j].id;
                  const opponentEnergy = this.pos[newY][newX].residents[j].energy;
                  if (animal.eats.includes(opponentId)) {
                    animal.energy += this.addEnergy(animal.level, opponentEnergy);
                    this.pos[newY][newX].residents.splice(j, 1);
                    animalAte = true;
                    break;
                  }
                }

                const plantId = this.pos[newY][newX].plant.id;
                if (!animalAte && animal.eats.includes(plantId)) {
                  animal.energy += this.addEnergy(animal.level, this.pos[newY][newX].plant.energy);
                  this.pos[newY][newX].plant.energy = 0;
                }
              }

              if (this.canReproduce(animal.level, animal.energy)) {
                this.pos[newY][newX].residents.push({  
                  color: animal.color,
                  level: animal.level,
                  name: animal.name,
                  id: animal.id,
                  eats: animal.eats,
                  energy: animal.energy / 3,
                  lastIteration: animal.lastIteration,
                  ownIteration: 0
                });
                animal.energy -= animal.energy / 3;
              }
            }
          }
        }

        this.lastUpdateDuration = performance.now() - startTime;
        this.averageUpdateDuration = (this.averageUpdateDuration * (this.iteration - 1) + this.lastUpdateDuration) / this.iteration;

        this.animalsKeys.forEach(k => this.animals[k].population = animalsPopulation[k]);
        this.plantsSumEnergy = 0;
        this.plantsKeys.forEach(k => this.plantsSumEnergy += plantsEnergy[k]);

        if (drawPlot) {
          this.animalsKeys.forEach(k => this.animalsEnergy[k].push(animalsEnergy[k]));
          this.plantsKeys.forEach(k => this.plantsEnergy[k].push(plantsEnergy[k]));
          this.refreshPlot();
        }

        this.totalTime = this.SPS * this.lastUpdateDuration
          + this.FPS * this.lastDrawDuration
          + (this.SPS / this.chartStep) * this.lastPlotDuration
      }

      if (this.SPS > 0) setTimeout(this.update, 1000 / this.SPS);
    },
    
    refreshPlot() {
      const startTime = performance.now();

      this.chartIteration += 1;

      const data = Object.keys(this.animalsEnergy).map(k => ({
        line: {
          color: this.animals[k].color,
          width: 3
        },
        name: k,
        x: Array.from(new Array(this.animalsEnergy[k].length), (_, i) => i * this.chartStep),
        y: this.animalsEnergy[k], 
        mode: 'lines'
      }));

      if (this.showProducers) {
        Object.keys(this.plantsEnergy).forEach(k => {
          data.push({
            line: {
              dash: 'dashdot',
              color: this.plants[k].color,
              width: 3
            },
            name: k,
            x: Array.from(new Array(this.plantsEnergy[k].length), (_, i) => i * this.chartStep),
            y: this.plantsEnergy[k], 
            mode: 'lines'
          });
        });
      }

      var layout = {
        xaxis: {
          title: 'Iteration'
        },
        yaxis: {
          title: 'Total species energy'
        },
        showlegend: false,
        autosize: false,
        height: 480,
        width: 1668,
        margin: {
          l: 42,
          r: 10,
          b: 35,
          t: 10,
          pad: 0
        }
      };

      if (this.showWholeChart) {
        if (this.chartIteration === 1) Plotly.newPlot('chart', data, layout);
        else Plotly.react('chart', data, layout);
      } else {
        data.forEach(d => {
          d.y = d.y.slice(-1 * this.chartLength / this.chartStep);
          d.x = d.x.slice(-1 * this.chartLength / this.chartStep);
        });
        if (this.chartIteration === 1) Plotly.newPlot('chart', data, layout);
        else Plotly.react('chart', data, layout);
      }

      if (this.chartIteration > 1) {
        this.lastPlotDuration = performance.now() - startTime;
        this.averagePlotDuration = (this.averagePlotDuration * (this.chartIteration - 1) + this.lastPlotDuration) / this.chartIteration;
      }
    },

    create2DArray(rows) {
      var arr = [];
      for (var i = 0; i < rows; i++) arr[i] = [];
      return arr;
    },

    hexToVBColor(rgb) {
      return Number(rgb.replace("#", "0x"));
    },

    getNewPosition(y, x, level, iteration) {
      let newY = 0;
      let newX = 0;

      if (iteration % (6 - level) !== 0) {
        newY = y;
        newX = x;
      } else {
        newY = y + (Math.floor(Math.random() * 2) === 1 ? 1 : -1);
        newX = x + (Math.floor(Math.random() * 2) === 1 ? 1 : -1);
        if (newY < 0 || newY >= this.height) newY = y;
        if (newX < 0 || newX >= this.width) newX = x;
      }
      
      return { newY, newX };
    },

    getStartEnergy(level) {
      return (level == 0 ? 7 : 2) * 100;
    },

    useEnergy(level) {
      // https://www.wolframalpha.com/input/?i=linear+fit+1000,+950,+890,+820,+750
      return this.getStartEnergy(level) / ((1071 - 63 * level) * 9);
    },

    addEnergy(level, energy) {
      // https://www.wolframalpha.com/input/?i=linear+fit+0.1,13.3,22.3,37.5
      return energy * (12.12 * level - 12) / 100;
    },

    addSolarEnergy() {
      return this.solarPower * 0.2 * 100;
    },

    canReproduce(level, energy) {
      return Math.random() * 500 * (level ** 3) < 1;
    }
  }
});
